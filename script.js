let calculateAnswer = () => {
	let expression = document.getElementById("inputBox").value;
	let degreeMode = document.getElementById("degree-checkbox").checked;
	if (expression !== "") {
		try {
			expression = calculateResin(expression);
			if (degreeMode) {
				expression = convertToRadian(expression);
			}
			answer = math.evaluate(expression);
			if (typeof answer !== "function") {
				document.getElementById("answerParagraph").innerText = answer;
				document.getElementById("answerParagraph").style.background = "#fff";
                document.getElementById("err-label").innerText = "";
			}
		} catch {
			document.getElementById("answerParagraph").style.background = "#ff000052";
            document.getElementById("err-label").innerText = "Invalid Input!";
		}
	} else {
		document.getElementById("answerParagraph").innerText = "0";
		document.getElementById("answerParagraph").style.background = "#fff";
        document.getElementById("err-label").innerText = "";

	}
};

let calculateResin = (expn) => {
    return expn.replace(/resin/g,"392.9");
};

let convertToRadian = (expn) => {
	expn = expn.replace(/sin\(/g, "sin(pi/180*");
	expn = expn.replace(/cos\(/g, "cos(pi/180*");
	return expn;
};

document.getElementById("inputBox").addEventListener("input", calculateAnswer);

document
	.getElementById("degree-checkbox")
	.addEventListener("change", calculateAnswer);
